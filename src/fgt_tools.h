#ifdef _WIN32
	#include <windows.h>
	unsigned long findAddr( void* functionAddress );
#elif defined( POSIX )
	unsigned long findAddr( unsigned long functionAddress );
	bool ptraceProtectionEnabled();
#else
	#error "Unknown OS."
#endif
