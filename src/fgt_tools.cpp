#ifdef _WIN32
	#include <Windows.h>
	#include <Tlhelp32.h>
	#include <conio.h>
	#pragma GCC diagnostic ignored "-Wpointer-arith" // This pragma ignores `pointer of type 'void *' used in arithmetic`

#elif defined( POSIX )
	#define _FILE_OFFSET_BITS 64 // Needs to be defined for fseeko to work with /proc/pid/mem
	#include <unistd.h>
	#include <fstream> // file io
	#include <cstdlib> // atoi
	#include <cstdio>
	#include <string.h> // memset
	#include <errno.h>
	#include <sys/ptrace.h>
	#include <sys/wait.h>
	#include <list>
#else
	#error "Unknown OS."
#endif

#ifdef _WIN32 // declare findAddr and findInRegion for windows. (Using VirtualQueryEx and ReadProcessMemory.)
// Scan through all the memory regions for this pointer.
unsigned long findInRegion( void* functionAddress, void* startAddress, unsigned long int scanSize ){
	HANDLE currentProc = GetCurrentProcess();
	BYTE* procMemory = new BYTE[scanSize];
	unsigned long int bytesRead;
	int found = 0;

	//_cprintf( "StartAddress = 0x%x\t ScanSize = %i\n", startAddress, scanSize );
	bool readSuccess = ReadProcessMemory( currentProc, (void*)startAddress, (void*)procMemory, scanSize, &bytesRead );
	if( !readSuccess ){
		_cprintf( "ReadProcessMemory failed: GetLastError() = %i\n", GetLastError() );
		return -1;
	}
	
	for(int i=0; i<bytesRead; ++i){// For loop searching all process memory
		if( procMemory[i] == ((int)functionAddress & 0xFF ) ){
			found = 1;
			
			if( !(procMemory[i+1] == (((int)functionAddress & 0xFF00)>>8)) ){ // I tried to put this in a for loop. Too awkward.
					found = 0;
					continue;
			}
			if( !(procMemory[i+2] == (((int)functionAddress & 0xFF0000)>>16)) ){
					found = 0;
					continue;
			}
			if( !(procMemory[i+3] == (((int)functionAddress & 0xFF000000)>>24)) ){
					found = 0;
					continue;
			}
			
			if(found){
				_cprintf( "Found a match at:\t0x%x\n", startAddress + i );
				free(procMemory);
				return (int)startAddress + i;
			}
		}
	}
	//_cprintf( "Failed to find address.\tLast Address: 0x%x\n", startAddress+bytesRead );
	free( procMemory );
	return 0;
}

unsigned long findAddr( void* functionAddress ){
	_cprintf( "Getting memory regions:\n" );
	HANDLE currentProc = GetCurrentProcess();
	const void* currentAddress = 0; // All memory regions.
	MEMORY_BASIC_INFORMATION memRegion; // This will hold the info on each memory region.
	
	//See ValueFinder.pas TValueFinder.create()
	while( (VirtualQueryEx(currentProc, currentAddress, &memRegion, sizeof( memRegion ))!=0) && ((currentAddress + memRegion.RegionSize)>currentAddress) ){
		if( (memRegion.State != MEM_COMMIT) || (memRegion.AllocationProtect != PAGE_READWRITE) ||
		 (memRegion.Protect != PAGE_READWRITE) || (memRegion.Type != MEM_PRIVATE) || (memRegion.RegionSize != 0x20000) ){ // The C pointers are always in this type of page.
			currentAddress = memRegion.BaseAddress + memRegion.RegionSize;
			continue;
		}
		//_cprintf( "  BaseAddr: 0x%x\t\t Size: 0x%x\n", memRegion.BaseAddress, memRegion.RegionSize );
		unsigned long int returnAddr = findInRegion( functionAddress, memRegion.BaseAddress, memRegion.RegionSize );
		if( returnAddr == -1 ){
			_cprintf( "ReadProcessMemory failed, aborting." );
			return 0;
		}
		if( returnAddr != 0 ){
			return returnAddr;
		}
		currentAddress = memRegion.BaseAddress + memRegion.RegionSize;
	}
	_cprintf( "Cant find address.\n" );
	return 0;
}


// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~
// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~ END OF WIN32 DECLARATION *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~
// *~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~
#elif defined( POSIX ) // Declare findAddr and findInRegion for linux(and darwin?) Using /proc/self/mem and ptrace.

using namespace std;

class memRegion {
    public:
        memRegion( long a, long b, long c );
        long startAddr;
        long endAddr;
        long size;
};

memRegion::memRegion( long a, long b, long c ){
        startAddr = a;
        endAddr = b;
        size = c;
}

FILE *memFile = NULL;
memRegion *vtableRegion = NULL;
list<memRegion> regionsList;

// /proc/sys/kernel/yama/ptrace_scope https://wiki.ubuntu.com/SecurityTeam/Roadmap/KernelHardening#ptrace%20Protection
bool ptraceProtectionEnabled(){
        if( access( "/proc/sys/kernel/yama/ptrace_scope", F_OK ) == 0 ){
                ifstream ptraceScope( "/proc/sys/kernel/yama/ptrace_scope" );
                if( ptraceScope.is_open() ){
                        string line;
                        getline( ptraceScope, line );
                        int ptraceValue = atoi( line.c_str() );
                        if( ptraceValue ){
                                //printf( "Your kernel has ptrace scoping enabled, you need to disable it to use this module. `echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope`\n" );
                                return true;
                        }
                } else {
                        //printf( "Your kernel may have ptrace scoping enabled, disable it with `echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope`\n" );
                        return true;
                }
        }
        return false;
}

unsigned long findInRegion( unsigned long searchAddr, unsigned long startAddr, unsigned long regionSize ){
        if( memFile == NULL ){
                printf( "/proc/pid/mem failed to open!\n" );
                return 0;
        }

        unsigned char *buffer = (unsigned char*)calloc( regionSize + 1, sizeof(char) );
        if( buffer == NULL ){
                printf( "Failed to allocate %u bytes for findInRegion buffer\n", regionSize + 1 );
                free( buffer );
                return 0;
        }

        if( fseeko( memFile, startAddr, SEEK_SET ) == -1 ){ // returns off_t (must use this to seek larger than 32bit)
                //perror( "fseek() failed " );
                printf( "findInRegion fseek() failed! [errno %i]\n", errno );
                free( buffer );
                return 0;
        }

        size_t readElements = fread( buffer, regionSize, 1, memFile );
        if( readElements != 1 ){
                perror( "findInRegion fread() failed! " );
                //printf( "findInRegion fread() failed! [errno %i]\n", errno );
                free( buffer );
                return 0;
        }

        bool found = false;
        unsigned long foundAddr = 0;
        for( int i = 0; i < regionSize; ++i ){
                if( buffer[i] == (searchAddr & 0xFF) ){ // Initial match
                        //printf( "Initial match found at 0x%x\n", startAddr + i );
                        found = true;

                        if( buffer[i+1] != ((searchAddr & 0xFF00)>>8) ){
                                //printf( "2nd 0x%x != 0x%x [%s]\n", buffer[i+1], ((searchAddr & 0xFF00)>>8), (buffer[i+1] != ((searchAddr & 0xFF00)>>8)) ? "true" : "false" );
                                found = false;
                                continue;
                        }
                        if( buffer[i+2] != ((searchAddr & 0xFF0000)>>16) ){
                                //printf( "3rd 0x%x != 0x%x [%s]\n", buffer[i+2], ((searchAddr & 0xFF0000)>>16), (buffer[i+2] != ((searchAddr & 0xFF0000)>>16)) ? "true" : "false" );
                                found = false;
                                continue;
                        }
                        if( buffer[i+3] != ((searchAddr & 0xFF000000)>>24) ){
                                //printf( "4th 0x%x != 0x%x [%s]\n", buffer[i+3], ((searchAddr & 0xFF000000)>>24), (buffer[i+3] != ((searchAddr & 0xFF000000)>>24)) ? "true" : "false" );
                                found = false;
                                continue;
                        }

                        if( found ){
                                printf( "Full match found at 0x%x\n", startAddr + i );
				return startAddr + i;
                                //foundAddr = startAddr + i;
                        }
                }
        }

        free( buffer );
        return foundAddr;
}

unsigned long findAddr( unsigned long searchAddr ){
        char memPath[128];
        char mapPath[128];
        char *mapLine = NULL;
        size_t lineSize = 0;;
        FILE *mapsFile = NULL;
        int usefulRegions = 0;
        int regionCounter = 0;


        if( regionsList.size() <= 0 ){
                printf( "Initializing regionsList\n" );
                snprintf( mapPath, sizeof( mapPath ), "/proc/%u/maps", getpid() );
                mapsFile = fopen( mapPath, "r" );

                if( mapsFile == NULL ){
                        printf( "Failed to open %s\n", mapPath );
                        return 0;
                }

                while( getline( &mapLine, &lineSize, mapsFile ) != -1 ){
                        unsigned long startAddr, endAddr;
                        char read, write, exec, cow, *regionName;
                        int offset, dev_major, dev_minor, inode;

                        regionName = (char*)calloc( lineSize, sizeof( char ) );
                        if( regionName == NULL ){
                                printf( "failed to allocate %lu bytes for regionName\n", (unsigned long int)lineSize );
                                free( regionName );
                                return 0;
                        }
                        memset( regionName, '\0', lineSize );

                        if( sscanf( mapLine, "%lx-%lx %c%c%c%c %x %x:%x %u %s", &startAddr, &endAddr, &read, &write, &exec, &cow, &offset, &dev_major, &dev_minor, &inode, regionName ) >= 6 ){
                                regionCounter++;
                                if( ((endAddr - startAddr) > 0) && (read == 'r') && (write == 'w') && (regionName[0] == '\0') ){
                                        //printf( "Useful region found at 0x%x [Size: %ul] %s | %s", startAddr, (endAddr-startAddr), (regionName != '\0') ? regionName : "undefined", mapLine );
                                        memRegion region = memRegion( startAddr, endAddr, (endAddr-startAddr) );
                                        regionsList.push_back( region );
                                        usefulRegions++;
                                }
                        }
                        free( regionName );
                }
                printf( "%i/%i regions are useful.\n", usefulRegions, regionCounter );
                fclose( mapsFile );
        }

	//ptrace( PTRACE_ATTACH, getpid() );
	//waitpid( getpid(), NULL, 0 );

        if( memFile == NULL ){ // We should be able to use /proc/self/mem ?
                snprintf( memPath, sizeof( memPath ), "/proc/%u/mem", getpid() );
                memFile = fopen( memPath, "rb" );
                if( memFile == NULL ){
                        printf( "Failed to open %s\n", memPath );
                        return 0;
                }
        }

	unsigned long found = 0;
	if( vtableRegion == NULL ){
		list<memRegion>::iterator iter;
		/*for( iter=regionsList.begin(); iter != regionsList.end(); ++iter ){
			printf( "Scanning Through Region - Start: 0x%x \t End: 0x%x \t Size: %ld\n", iter->startAddr, iter->endAddr, iter->size );
			found = findInRegion( searchAddr, iter->startAddr, iter->size );
			if( found != 0 ){
				break;
			}
		}*/
		for( iter=regionsList.end(); iter != regionsList.begin(); --iter ){
			printf( "Scanning Through Region - Start: 0x%x \t End: 0x%x \t Size: %ld\n", iter->startAddr, iter->endAddr, iter->size );
			found = findInRegion( searchAddr, iter->startAddr, iter->size );
			if( found != 0 ){
				vtableRegion = new memRegion( iter->startAddr, iter->endAddr, iter->size );
				break;
			}
		}
	} else {
		found = findInRegion( searchAddr, vtableRegion->startAddr, vtableRegion->size );
		if( found == 0 ){
			printf( "findInRegion failed on vtableRegion! Clearing vtableRegion and recursing all regions.\n" );
			vtableRegion = NULL;
			found = findAddr( searchAddr );
			if( found == 0 ){
				printf( "failed to findAddr even after recursing all regions!!!\n" );
			}
		}	
	}

        fclose( memFile );
	memFile = NULL;
        //ptrace( PTRACE_DETACH, getpid() );
        return found;
}

#else
	#error "Unknown OS."
#endif
