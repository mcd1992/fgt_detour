/*
   CFunc fgtdetour.Detour( targetToDetour, ourDetour )
		The bread and butter. Returns a function to the original targetToDetour.
		This will make targetToDetour call ourDetour in lua. Make sure to keep a variable to the original function.
   bool fgtdetour.RemoveDetour( targetToRemove )
		This will remove a detour, if it exists, to targetToRemove.
		Call this at the beginning of your scripts if you want to overwrite any old detours.
   void fgtdetour.PrintDetouredFunctions()
		Print the kv map of all the currently detoured functions.
   void fgtdetour.RemoveAllDetours()
		Remove every detoured function and cleanup all references.
*/

#undef _UNICODE
#define GMMODULE

#ifdef POSIX
	#define LINUX
	#define _LINUX
	#define GNUC
	#define Sleep( ... ) usleep( __VA_ARGS__ )
	#define AllocConsole()
	#define _cprintf( ... ) printf( __VA_ARGS__ )
	#define VSTD_LIB "libvstdlib.so" // /proc/pid/maps
	#include <unistd.h>
	#include <string.h> // strstr
	#include <stdarg.h> // va_args
#elif defined( _WIN32 )
	#define VSTD_LIB "vstdlib.dll"
	#include <Windows.h>
	#include <conio.h>
	#pragma comment (linker, "/NODEFAULTLIB:libcmt")
#else
	#error "Unknown OS."
#endif

#include "GarrysMod/Lua/Interface.h"
#include "fgt_tools.h"
#include <cstdio>
#include <cstdlib>
#include <map>

#pragma GCC diagnostic ignored "-Wwrite-strings" // This pragma ignores `depreciated conversion from string constant to char*`

using namespace GarrysMod::Lua;

class LuaVTable {
	public:
		char unknown1[20]; // 0x14
		CFunc cFuncAddr; // 0x4
		char unknown2[24]; // 0x18
		char functionName[16]; // Everything until the next null is the function name. 0x10 16 bytes is always allocated.
};

class DetourInfo {
	public:
		int ourDetourRef; // This is the integer reference key to our target lua function to call on detour. (Function is stored in registry)
		
		LuaVTable* luaFunctionVTable; /* This is the struct to the lua VTable.
		(The address of this matches the address of tostring( callingFunction ) in lua)
		(The address of this is also the address of detouredFunctions keys)*/
		
		CFunc originalCFunction; // This is the original C functions address/callable. (originalLocation/Address is used in RemoveDetour())
		
		DetourInfo( int, LuaVTable*, CFunc );
};

DetourInfo::DetourInfo( int a, LuaVTable* b, CFunc c ){
	ourDetourRef = a;
	luaFunctionVTable = b;
	originalCFunction = c;
}

std::map <unsigned long int, DetourInfo*> detouredFunctions; // K = "0x12345678", V = DetourInfo( 0x12345678, 1337, ... )
std::map <unsigned long int, DetourInfo*>::iterator mapIter; // Iterator function
char FormattedText[2048]; // Char array to hold our formatted text.

// Convert a string like 0x12345678 to unsigned long int.
long unsigned int hexstrToInt( const char* hexstring ){
	if( strstr( hexstring, "function: " ) ){
		hexstring = &hexstring[10]; // this is from lua's tostring: `function: 0xaddress` strip the `function: `
	}
	if( hexstring[0] != '0' || hexstring[1] != 'x' ){
		_cprintf( "failed to hexstrToInt( %s )", hexstring );
		return 0;
	}
	return strtoul( hexstring, NULL, 16 );
}

// Internal function for printing to the game console. Like print( string.format() )
int con_printf( lua_State* state, const char* formatString, ... ){ // Be careful about the state.
	//_cprintf( "con_printf entry stack size %i\n", LUA->Top() );
	
	va_list formatArgs; // Use <cstdio>'s varargs to pass N arguments to vsprintf()
	va_start( formatArgs, formatString );
	vsprintf( FormattedText, formatString, formatArgs );
	va_end( formatArgs );

	unsigned int length = strlen( FormattedText );
	LUA->PushSpecial( SPECIAL_GLOB );	// _G
		LUA->GetField( -1, "print" );	// _G.print
		LUA->PushString( FormattedText, length );	// first argument
		LUA->Call( 1, 0 );				// _G.print( arg1 );
	LUA->Pop();							// Pop _G
	
	//_cprintf( "con_printf exit stack size %i\n", LUA->Top() );
	return 0;							// No arguments to return
}

// This is our function that gets called by every detoured function.
int overloadedDetour( lua_State* state ){ // TODO: use lua_tolstring to get the function address. ditch hexstrToInt.
	//_cprintf( "\noverloadedDetour entry stack size %i\n", LUA->Top() );
	//_cprintf( "return address = 0x%x\n", __builtin_return_address(1) );
	
	LUA->PushSpecial( SPECIAL_GLOB ); // This tree of lua shit gets the calling functions original address. tostring( debug.getinfo( 1, "f" ).func )
		LUA->GetField( -1, "debug" );
		LUA->GetField( -1, "getinfo" ); // Stack is now Top: getinfo, debug, _G :Bottom
		LUA->PushNumber( 1 );
		LUA->PushString( "f" );
		LUA->Call( 2, 1 ); // 2 arguments, 1 return value.
		LUA->GetField( -1, "func" );
		LUA->GetField( -4, "tostring" );
		LUA->Insert( -2 ); // Move tostring to before func
		LUA->Call( 1, 1 );
		const char* callFuncTostring = LUA->GetString( -1 );
	LUA->Pop( 4 ); // tostring return, getinfo return, debug, _G

	unsigned long int callingFuncInitialAddr = hexstrToInt( callFuncTostring );
	if( detouredFunctions[ callingFuncInitialAddr ] != NULL ){
		int ourDetourRef = detouredFunctions[ callingFuncInitialAddr ]->ourDetourRef;
		LUA->ReferencePush( ourDetourRef );
		LUA->Insert( 1 ); // Move our function to the bottom of the stack.
		LUA->Call( LUA->Top()-1, -1 ); // Call the function with the original arguments. (LUA_MULTRET = -1, (Return all arguments))
	} else {
		// This should never happen >_>
		LUA->ThrowError( "overloadedDetour called without a matching detour!" ); // Throw an error and print a stacktrace
	}
	
	//_cprintf( "overloadedDetour exit stack size %i\n", LUA->Top() );
	return LUA->Top(); // Return how many arguments our detour returned.
}

// void fgtdetour.PrintDetouredFunctions()
// Prints the detouredFunctions map into your console
int PrintDetouredFunctions( lua_State* state ){
	con_printf( state, "detouredFunctions has %i elements.", detouredFunctions.size() );
	for( std::map<unsigned long int, DetourInfo*>::iterator mapIter=detouredFunctions.begin(); mapIter != detouredFunctions.end(); ++mapIter ){
		unsigned long int key = mapIter->first;
		DetourInfo* value = mapIter->second;
		con_printf( state, "%u = [ ourDetourRef = %i, LuaVTable = 0x%x, originalCFunction = 0x%x ](%s)", key,
			value->ourDetourRef, value->luaFunctionVTable, value->originalCFunction, value->luaFunctionVTable->functionName );
	}
	return 0;
}

// void fgtdetour.RemoveAllDetours()
// Removes all the detoured functions, setting them back to the original c functions
int RemoveAllDetours( lua_State* state ){
	con_printf( state, "Removing %i detours:", detouredFunctions.size() );
	for( std::map<unsigned long int, DetourInfo*>::iterator mapIter=detouredFunctions.begin(); mapIter != detouredFunctions.end(); ++mapIter ){
		unsigned long int key = mapIter->first;
		DetourInfo* value = mapIter->second;
		con_printf( state, "   Removing %s", value->luaFunctionVTable->functionName );
		value->luaFunctionVTable->cFuncAddr = (CFunc)value->originalCFunction; // Point the VTable back to the original C function.
		LUA->ReferenceFree( value->ourDetourRef ); // Remove our function from the lua registry.
	}
	detouredFunctions.clear(); // Remove all detours
	return 0;
}

// bool fgtdetour.RemoveDetour( targetFunction )
// Remove a detour if it exists. Returns true on remove, false if it isn't detoured (or error?).
int RemoveDetour( lua_State* state ){
	LUA->CheckType( 1, Type::FUNCTION );
	
	LUA->PushSpecial( SPECIAL_GLOB ); // use tostring( function ) to get the vtable address since we don't have acceess to index2adr
		LUA->GetField( -1, "tostring" );
		LUA->Push( 1 ); // Push the bottom of the stack, targetFunction, to the top. (This pops index 1)
		LUA->Call( 1, 1 );
		const char* targetFuncStr = LUA->GetString( -1 );
	LUA->Pop(); // _G
	
	unsigned long int targetFuncAddr = hexstrToInt( targetFuncStr );
	DetourInfo* thisDetour = detouredFunctions[ targetFuncAddr ];
	if( thisDetour != NULL ){
		LuaVTable *targetFuncVT = thisDetour->luaFunctionVTable;
		_cprintf( "Removing detour of %s\n", targetFuncVT->functionName );
		targetFuncVT->cFuncAddr = (CFunc)thisDetour->originalCFunction; // Change VTable to point back to the original C function
		LUA->ReferenceFree( thisDetour->ourDetourRef ); // Free the reference to our detour in lua
		detouredFunctions.erase( targetFuncAddr ); // Delete the kv map
		LUA->PushBool( true );
	} else {
		LUA->PushBool( false );
	}
	return 1;
}

// CFunc fgtdetour.Detour( targetFunction, myFunction )
// Creates a detour of targetFunction to call myFunction. Returns the original cfunction or nil
int CreateDetour( lua_State* state ){ 
	_cprintf( "\nCreateDetour entry stack size %i\n", LUA->Top() );
	LUA->CheckType( 1, Type::FUNCTION ); // Check if arguments are functions.
	LUA->CheckType( 2, Type::FUNCTION );
	
	CFunc targetFunction = LUA->GetCFunction( 1 ); // Argument 1 (targetFunction)
	int myFunctionRef = LUA->ReferenceCreate(); // Push argument 2 into the registry w/ reference (This pops the stack)
	LUA->Pop(); // Pop targetFunction off stack (Stack is now empty)
	
	if( targetFunction == NULL ){
		con_printf( state, "[ERROR] argument #1 to 'Detour' is not a C defined function." );
		LUA->PushNil();
		return 1;
		// TODO: Print debug.getinfo( targetFunction, "S" ).short_src
	}
	
	// TODO: Delete this debug code
	// con_printf( state, "PID = %i", getpid() );
	// con_printf( state, "function addr = 0x%x\n", targetFunction );
	// return 0;

	unsigned long int targetFunctionVTLoc = findAddr( (unsigned long int)*targetFunction );
	if( targetFunctionVTLoc == 0 ){
		con_printf( state, "Failed to find targetFunction in Lua's VTable!" );
		LUA->PushNil();
		return 1;
	}
	
	unsigned long int targetFuncIntAddr = (long unsigned int)((CFunc)(targetFunctionVTLoc-0x14)); // cast to be used as the key in detouredFunctions map.
	if( detouredFunctions[ targetFuncIntAddr ] != NULL ){
		con_printf( state, "[ERROR] argument #1 to 'Detour' is already detoured." ); // TODO: Find out which script called Detour() on this first.
		LUA->PushNil();
		return 1;
	}
	detouredFunctions[ targetFuncIntAddr ] = new DetourInfo( myFunctionRef, (LuaVTable*)(targetFunctionVTLoc-0x14), (CFunc)targetFunction );
	
	LuaVTable *targetFuncVT = (LuaVTable*)(targetFunctionVTLoc-0x14);
	_cprintf( "Modifying Lua VTable: %s's old address is 0x%x\n", targetFuncVT->functionName, targetFuncVT->cFuncAddr );
	targetFuncVT->cFuncAddr = (CFunc)overloadedDetour;
	
	LUA->PushCFunction( (CFunc)targetFunction ); // Return the old C function to be called by the new detour.
	
	_cprintf( "CreateDetour exit stack size %i\n", LUA->Top() );
	return 1;
}

GMOD_MODULE_OPEN(){
#ifdef POSIX
	if( ptraceProtectionEnabled() ){
		const char title[] = "Your system has ptrace protection enabled!";
		const char text[] = "You need to disable ptrace_scope in order to use this module!\n'echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope'\n";
		char zenityCommand[256];
		snprintf( zenityCommand, sizeof( zenityCommand ), "zenity --error --title=\"%s\" --text=\"%s\" > /dev/null 2>&1 &", title, text ); // popup forked error dialog and redirect stderr and stdout to null
		system( zenityCommand );
		con_printf( state, text );
		printf( text );
		return 0;
	}
#endif
	//AllocConsole();
	LUA->PushSpecial( SPECIAL_GLOB ); // If DEBUGFGTDETOUR = true then AllocConsole()
		LUA->GetField( -1, "DEBUGFGTDETOUR" );
		if( LUA->GetType( -1 ) == Type::BOOL ){
			bool allocDebugCon = LUA->GetBool( -1 );
			if( allocDebugCon ){
				AllocConsole();
				Sleep( 250 ); // Give some time for the console to allocate.
			}
		}
	LUA->Pop( 2 ); // _G, DEBUGFGTDETOUR
	_cprintf( "\n\n\n\n\n\nGMOD_MODULE_OPEN\n" );
	_cprintf( "overloadedDetour = 0x%x\n", overloadedDetour );
	_cprintf( "GMOD_MODULE_OPEN Entered with %i objects on the stack.\n", LUA->Top() );
	
	// setup fgtdetour global table
	LUA->PushSpecial( SPECIAL_GLOB );
		LUA->CreateTable();
		LUA->SetField( -2, "fgtdetour" ); // _G["fgtdetour"] = CreateTable() (This pops the stack)
		LUA->GetField( -1, "fgtdetour" ); // Keep fgtdetour on the stack
		
		LUA->PushCFunction( CreateDetour );
		LUA->SetField( -2, "Detour" ); // _G.fgtdetour.Detour()
		
		LUA->PushCFunction( RemoveDetour ); // _G.fgtdetour.RemoveDetour()
		LUA->SetField( -2, "RemoveDetour" );
		
		LUA->PushCFunction( PrintDetouredFunctions ); // _G.fgtdetour.PrintDetouredFunctions()
		LUA->SetField( -2, "PrintDetouredFunctions" );
		
		LUA->PushCFunction( RemoveAllDetours ); // _G.fgtdetour.RemoveAllDetours()
		LUA->SetField( -2, "RemoveAllDetours" );
	LUA->Pop( 2 ); // fgtdetour, _G
	
	_cprintf( "GMOD_MODULE_OPEN Exited with %i objects on stack.\n", LUA->Top() );
	return 0;
}

GMOD_MODULE_CLOSE(){
	_cprintf( "GMOD_MODULE_CLOSE (%i objects on stack)\n", LUA->Top() );
	_cprintf( "Removing all detours.\n" );
	RemoveAllDetours( state );
	//MessageBox( NULL, "GMOD_MODULE_CLOSE", NULL, MB_OK );
	return 0;
}
